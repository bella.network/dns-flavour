#!/bin/sh

# List of given files which should be precompressed
for singleFile in generated/lists/*.txt; do
	if [ -f "${singleFile}" ]; then
		echo "Compressing $singleFile"
		cat "$singleFile" | gzip - > "${singleFile}.gz"
		touch -r "$singleFile" "${singleFile}.gz"
		cat "$singleFile" | brotli - > "${singleFile}.br"
		touch -r "$singleFile" "${singleFile}.br"
	fi
done
