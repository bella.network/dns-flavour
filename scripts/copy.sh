#!/bin/sh

mkdir generated/lists || true
mkdir generated/lists/specific || true

# copy specific lists (non collections, contains only lists defined manually within this repo)
rsync -vaP blocklists generated/lists/specific/
rsync -vaP whitelists generated/lists/specific/

# generate folder structure for collection
mkdir generated/data/ || true
mkdir generated/collections || true
mkdir generated/collections/blocklists || true
mkdir generated/collections/whitelists || true
